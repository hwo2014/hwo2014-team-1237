﻿import json
import socket
import sys
import random
import math

#NoobBot goes bubbelgumz dibbed in spagetti
class NoobBot(object):

    def __init__(self, socket, name, key):
        #my precious
        self.socket = socket
        self.name = name
        self.key = key
        self.inPiece = None
        self.lastPieceDistance = None
        self.lastSpeed = 0
        self.lastAngle = None
        self.angleChange = 0.0
        self.track = None
        self.lanes = None
        self.lastLap = 0
        self.turbos = False
        self.turboLength = 0
        self.turboStart = -1
        self.gameTick = 0
        self.switches = dict()
        self.switchDone = False
        #self.f = open(str(self.name)+'.txt','a+')
        self.throttl = 1.0
        self.sforceLimit = 0.45 #int(random.uniform(0.52,0.56) * 100) / 100.0
        self.speedLimit = 12 #random.randrange(13,17,1)
        self.maxAngle = 0.0
        self.chillage = 0
        self.bestLap = 60000
        self.traction = 0
        self.sentForce = 0
        self.turboNext = -1
        self.switchNext = 0

    def msg(self, msg_type, data):
        #if msg_type == "join":
            #self.send(json.dumps({"msgType": "joinRace", "data": { "botId": data, "trackName": "france", "carCount": 1}}))
            #self.send(json.dumps({"msgType": "createRace", "data": { "botId": data, "trackName": "usa", "password": "lolz", "carCount": 8}}))
        #    self.send(json.dumps({"msgType": "joinRace", "data": { "botId": data, "trackName": "suzuka", "password": "foo", "carCount": 1}}))
        #else:
		self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
		self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
		self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()
        self.ping()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def analyze_track(self, data):
		self.track = data['race']['track']['pieces']
		#self.lastLap = data['race']['raceSession']['laps']-1
		self.lanes = data['race']['track']['lanes']
		lol = 0
		kountteri = 0
		for piece in self.track:
			if 'switch' in piece:
				for i in range(lol+1,len(self.track)):
					print str(lol) + " " + str(kountteri)
					if i == len(self.track) - 1:
						self.switches[lol] = self.switches[min(self.switches)]
					elif('angle' in self.track[i]):
						kountteri = kountteri + self.track[i]['angle']*self.track[i]['radius']
					if('switch' in self.track[i]):
						if(kountteri > 0):
							self.switches[lol] = "Right"
							kountteri = 0
						elif(kountteri < 0):
							self.switches[lol] = "Left"
							kountteri = 0
						elif(kountteri == 0):
							rando = random.random()
							if(rando > 0.5):
								self.switches[lol] = "Right"
							else:
								self.switches[lol] = "Left"
						else:
							kountteri = 0
						break
				if(lol+1 == len(self.track)):
					self.switches[lol] = self.switches[min(self.switches)]
			lol += 1
		print str(data) + " " + str(self.switches) + " " + str(self.lanes)

    def switch_track(self, data):
		self.msg("switchLane", data)
	
    def on_game_start(self, data):
        print("Race started")
        self.ping()
	
    def on_car_positions(self, data):
		carIndex = -1
		for i in range(0,len(data)):
			if data[i]['id']['name'] == self.name:
				carIndex = i
		if data[carIndex]['piecePosition']['pieceIndex'] == self.inPiece:
			self.lastSpeed = data[carIndex]['piecePosition']['inPieceDistance']-self.lastPieceDistance
		if self.lastAngle != None:
			self.angleChange = self.lastAngle - data[carIndex]['angle']
		laneDist = self.lanes[data[carIndex]['piecePosition']['lane']['endLaneIndex']]['distanceFromCenter']
		self.inPiece = data[carIndex]['piecePosition']['pieceIndex']
		self.lastPieceDistance = data[carIndex]['piecePosition']['inPieceDistance']
		self.lastAngle = data[carIndex]['angle']
		
		if('length' in self.track[self.inPiece]):
			self.sentForce = 0
				
		if 'radius' in self.track[(self.inPiece+1) % len(self.track)]:
			if self.track[(self.inPiece+1) % len(self.track)]['angle'] > 0:
				self.sentForce = (self.lastSpeed*self.lastSpeed)/(self.track[(self.inPiece+1) % len(self.track)]['radius']-laneDist)
			elif self.track[(self.inPiece+1) % len(self.track)]['angle'] < 0:
				self.sentForce = (self.lastSpeed*self.lastSpeed)/(abs(self.track[(self.inPiece+1) % len(self.track)]['radius'])+laneDist)

		if 'radius' in self.track[(self.inPiece+1) % len(self.track)] and self.lastSpeed != 0:
			if self.track[(self.inPiece+1) % len(self.track)]['angle'] > 0:
				self.traction = abs(self.lastAngle)/(self.lastSpeed*self.lastSpeed)/(self.track[(self.inPiece+1) % len(self.track)]['radius']-laneDist)
			elif self.track[(self.inPiece+1) % len(self.track)]['angle'] < 0:
				self.traction = abs(self.lastAngle)/(self.lastSpeed*self.lastSpeed)/(abs(self.track[(self.inPiece+1) % len(self.track)]['radius'])+laneDist)
		
		lateralForce = -self.lastSpeed*math.tan(self.angleChange*math.pi/180)
			
		#offensive magick
		for i in range(0,len(data)):
			if(data[i]['id']['name'] != self.name and data[i]['piecePosition']['pieceIndex'] == self.inPiece+1 and data[i]['piecePosition']['lane'] == data[carIndex]['piecePosition']['lane'] and self.turbos == True and 'length' in self.track[self.inPiece] and 'length' in self.track[(self.inPiece+1) % len(self.track)] and 'radius' in self.track[(self.inPiece+2) % len(self.track)] and self.track[(self.inPiece+2) % len(self.track)]['radius'] < 51 and self.lastSpeed > 6):
				print "bump!"
				self.turboStart = self.gameTick
				self.turbos = False
				self.msg("turbo","ulululuuuuuuuuuu!")
			if(data[i]['id']['name'] != self.name and ((data[i]['piecePosition']['pieceIndex'] == self.inPiece and data[i]['piecePosition']['inPieceDistance']>data[carIndex]['piecePosition']['inPieceDistance']) or (data[i]['piecePosition']['pieceIndex'] == self.inPiece+1) or (data[i]['piecePosition']['pieceIndex'] == self.inPiece+2)) and data[i]['piecePosition']['lane'] == data[carIndex]['piecePosition']['lane'] and self.turbos == True and self.inPiece == int(max(self.switches))-1):
				if(self.switches[self.inPiece+1] == "Right"):
					self.switch_track("Left")
				elif(self.switches[self.inPiece+1] == "Left"):
					self.switch_track("Right")
				self.switchDone = True
				self.turboNext = self.gameTick+1
			if(data[i]['id']['name'] != self.name and data[i]['piecePosition']['pieceIndex'] == self.inPiece and data[i]['piecePosition']['lane'] == data[carIndex]['piecePosition']['lane'] and data[i]['piecePosition']['inPieceDistance']>data[carIndex]['piecePosition']['inPieceDistance'] and data[i]['piecePosition']['inPieceDistance']-data[carIndex]['piecePosition']['inPieceDistance'] < 20):
				self.switchNext += 1
			#print str(data[i]['id']['name'] != self.name) + " " + str(data[i]['piecePosition']['pieceIndex'] == self.inPiece) + " " + str(data[i]['piecePosition']['lane'] == data[carIndex]['piecePosition']['lane']) + " " + str(data[i]['piecePosition']['inPieceDistance']>data[carIndex]['piecePosition']['inPieceDistance']) + " " + str(data[i]['piecePosition']['inPieceDistance']-data[carIndex]['piecePosition']['inPieceDistance'] < 10)

		if(self.switchNext > 5 and 'switch' in self.track[self.inPiece+2]):
			if(self.switches[self.inPiece+2] == "Right"):
				self.switch_track("Left")
				self.switchDone = True
				self.switchNext = 0
			elif(self.switches[self.inPiece+2] == "Left"):
				self.switch_track("Right")
				self.switchDone = True
				self.switchNext = 0
		if(self.turboNext == self.gameTick):
				print "overtake!"
				self.turboStart = self.gameTick
				self.turbos = False
				self.msg("turbo","ulululuuuuuuuuuu!")
				
	
		#turbo if applicable (after 3/4 of track), or switch lanes
		if('radius' in self.track[self.inPiece] and self.lastPieceDistance > self.track[self.inPiece]['radius']*self.track[self.inPiece]['angle']*math.pi/180*(float(1)/float(2)) and 'length' in self.track[(self.inPiece+1) % len(self.track)] and 'length' in self.track[(self.inPiece+2) % len(self.track)] and 'length' in self.track[(self.inPiece+3) % len(self.track)] and self.turbos == True and self.turboStart+self.turboLength < self.gameTick and (float(self.inPiece)/float(len(self.track))) > (float(3)/float(4))):
			print "turboo!"
			self.turboStart = self.gameTick
			self.turbos = False
			self.msg("turbo","ulululuuuuuuuuuu!")
		elif self.switchDone == False and 'switch' in self.track[(self.inPiece+1) % len(self.track)]:
			self.switchDone = True
			#print str(self.inPiece+1) + " " + str(self.switches[self.inPiece+1])
			self.switch_track(self.switches[self.inPiece+1])
		elif 'switch' in self.track[self.inPiece] and self.switchDone == True:
			self.switchDone = False

		#if bend inc after 4 piece long straight or after this lenght this+1 length radius, chill (+ lolololool esoteric magicx, bulldozing must go ooooon!)
		if(self.lastSpeed > 2 and ('length' in self.track[self.inPiece] and 'length' in self.track[(self.inPiece+1) % len(self.track)] and 'radius' in self.track[(self.inPiece+2) % len(self.track)] and 'radius' in self.track[(len(self.track)+(self.inPiece+3)) % len(self.track)] and self.track[(self.inPiece+2) % len(self.track)]['radius'] < 51 and self.track[(self.inPiece+3) % len(self.track)]['radius'] < 51 and self.gameTick > 50) or (self.gameTick > 50 and 'length' in self.track[(len(self.track)+(self.inPiece-1)) % len(self.track)] and 'length' in self.track[(len(self.track)+(self.inPiece-2)) % len(self.track)] and 'length' in self.track[self.inPiece] and 'length' in self.track[(self.inPiece+1) % len(self.track)] and 'radius' in self.track[(self.inPiece+2) % len(self.track)]) or ('angle' in self.track[self.inPiece] and self.track[self.inPiece]['radius'] > 50 and 'length' in self.track[(self.inPiece+1) % len(self.track)] and 'radius' in self.track[(self.inPiece+2) % len(self.track)] and self.track[(self.inPiece+2) % len(self.track)]['radius'] < 51 and ('length' in self.track[len(self.track)-1] and (self.track[len(self.track)-1]['length'] != 59 or self.track[len(self.track)-1]['length'] != 91)) )): # """"""
			if self.track[(self.inPiece+2) % len(self.track)]['angle'] > 0:
				self.throttl -= self.lastSpeed/(self.track[(self.inPiece+2) % len(self.track)]['radius']-laneDist)
			elif self.track[(self.inPiece+2) % len(self.track)]['angle'] < 0:
				self.throttl -= self.lastSpeed/(abs(self.track[(self.inPiece+2) % len(self.track)]['radius'])+laneDist)
			#print "rule1"
		#if turbo is on and goin too fast (and not end of last lap), chill
		elif((self.lastSpeed > self.speedLimit)):# and (data[carIndex]['piecePosition']['lap'] != self.lastLap  or (data[carIndex]['piecePosition']['lap'] == self.lastLap and (float(self.inPiece)/float(len(self.track))) < (float(2)/float(3)))) and self.lastSpeed > 2):
			if(self.turboStart > 0 and self.turboStart+self.turboLength > self.gameTick):
				self.throttl = 0
				self.chillage = 1
		#desperate!
		elif(self.lastSpeed > 8.5 and 'length' in self.track[self.inPiece] and 'radius' in self.track[(self.inPiece+2) % len(self.track)]):
			self.throttl -= self.lastSpeed/60
		#if next turn gonna have some edge, chill
		elif(self.lastSpeed > 2 and(self.gameTick > 50 and self.sentForce > self.sforceLimit-self.traction) or ('radius' in self.track[self.inPiece] and 'radius' in self.track[(self.inPiece+1) % len(self.track)] and 'radius' in self.track[(self.inPiece+2) % len(self.track)] and self.track[(self.inPiece+1) % len(self.track)]['radius'] > self.track[(self.inPiece+2) % len(self.track)]['radius'])): #0.37
			if 'radius' in self.track[(self.inPiece+1) % len(self.track)]:
				if self.track[(self.inPiece+1) % len(self.track)]['angle'] > 0:
					self.throttl -= self.lastSpeed/(self.track[(self.inPiece+1) % len(self.track)]['radius']-laneDist)
				elif self.track[(self.inPiece+1) % len(self.track)]['angle'] < 0:
					self.throttl -= self.lastSpeed/(abs(self.track[(self.inPiece+1) % len(self.track)]['radius'])+laneDist)
			elif(self.lastSpeed > 5.5):
				self.throttl -= self.lastSpeed/60
			else:
				self.throttl += 0.3
			#print "rule2"
		#if in straight and next is bend, chill
		elif(self.lastSpeed > 2 and (('length' in self.track[self.inPiece]) and ('radius' in self.track[(self.inPiece+1) % len(self.track)])) and ((self.track[self.inPiece]['length']-data[carIndex]['piecePosition']['inPieceDistance'] <(self.track[self.inPiece]['length']/2)))): # or self.lastSpeed > 8):
			if self.track[(self.inPiece+1) % len(self.track)]['angle'] > 0:
				self.throttl -= self.lastSpeed/(self.track[(self.inPiece+1) % len(self.track)]['radius']-laneDist)
			elif self.track[(self.inPiece+1) % len(self.track)]['angle'] < 0:
				self.throttl -= self.lastSpeed/(abs(self.track[(self.inPiece+1) % len(self.track)]['radius'])+laneDist)
			#print "rule3"
		elif(self.angleChange > 0.3 and self.lastAngle < -30.0) or (self.angleChange < -0.3 and self.lastAngle > 30.0) or ('radius' in self.track[self.inPiece] and lateralForce > 0.2):
			if(self.lastSpeed > 8):
				self.throttl = min((1-abs(lateralForce))-0.2, 1-self.sentForce-0.2)
			else:
				self.throttl = min((1-abs(lateralForce)), 1-self.sentForce)
			#print "rule4"
		else:
			if(self.chillage == 1):
				self.throttl = 0
			elif('radius' in self.track[self.inPiece]):
				self.throttl += (10-self.lastSpeed)/50
			else:
				self.throttl = 1
		
		if (self.throttl > 1):
			self.throttl = 1
		elif (self.throttl < 0):
			self.throttl = 0
		
		if(self.turboStart > 0 and self.turboStart+self.turboLength+1 < self.gameTick and self.chillage == 1):
			self.chillage = 0
		
		if(self.gameTick != self.turboStart):
			self.throttle(self.throttl)
		
		#if(self.sentForce != 0):
		#	print str(self.lastSpeed) + " " + str(self.lastAngle) + " " + str(self.angleChange) + " " + str(lateralForce) + " " + str(self.throttl) + " " + str(self.track[self.inPiece]) + " " + str(self.sentForce) + " " + str(self.traction) #+ " " + str(self.maxAngle) + " " + str(self.turbos) + " " + str(self.gameTick)
		#else:
		#	print str(self.lastSpeed) + " " + str(self.lastAngle) + " " + str(self.angleChange) + " " + str(lateralForce) + " " + str(self.throttl) + " " + str(self.track[self.inPiece]) + " " + str(self.traction) #+ " " + str(self.maxAngle) + " " + str(self.turbos) + " " + str(self.gameTick)
		
		self.maxAngle = max(abs(self.lastAngle), self.maxAngle)
		
    def on_crash(self, data):
		print("Someone crashed")
		if data['name'] == self.name: #here be some hillclimbing, or hillrolling
			if(self.gameTick < self.turboStart+self.turboLength+45 and self.speedLimit > 10):
				self.speedLimit -= 0.5
			else:
				self.sforceLimit -= 0.01
		self.throttl = 1.0
		self.ping()

    def on_turbo(self, data):
		self.turbos = True
		self.turboLength = data['turboDurationTicks']
		print data

    def on_lapfinish(self, data):
		if(data['car']['name'] == self.name):
			self.bestLap = min(self.bestLap, data['lapTime']['millis'])
			if(self.maxAngle < 30):
				self.sforceLimit += 0.04
			elif(self.maxAngle < 40):
				self.sforceLimit += 0.02
			elif(self.maxAngle < 50):
				self.sforceLimit += 0.01
				
				
		print data
		
    def on_game_end(self, data):
        print("Race ended")
        #self.f.write("end: " + str(self.sforceLimit) + " " + str(self.speedLimit)+ " " + str(self.bestLap) + " " + str(self.maxAngle)+"\n")
        print "end: " + str(self.sforceLimit) + " " + str(self.speedLimit)+ " " + str(self.bestLap) + " " + str(self.maxAngle)
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
			'gameInit': self.analyze_track,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
			'lapFinished': self.on_lapfinish,
			'turboAvailable': self.on_turbo,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if('gameTick' in msg):
               self.gameTick = msg['gameTick']
            if msg_type in msg_map:
                #print msg_type
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                print msg

            line = socket_file.readline()



if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key,)
        bot.run()
